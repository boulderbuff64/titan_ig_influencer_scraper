Authors
=======

The core instagram-scraper is written and maintained by Richard Arcega and can be found here: https://pypi.python.org/pypi/instagram-scraper

Influencer scraper was made by Andrew Erickson

Follow me on Instagram at @TheAndrewErickson
