#!/usr/bin/env python
# coding: utf-8

# In[5]:


import datetime
import json
import os
from pprint import pprint
import requests
import random

global output_log
output_log = ""
global output_post_text
output_post_text = ""


# In[6]:


def randomHashtags():
    with open("hashtags.txt") as f:
        hashtags = f.readlines()
        # Remove whitespace characters like `\n` at the end of each line
        hashtags = [x.strip() for x in hashtags] 
        hashtags = [x.replace('#', '') for x in hashtags] 
    
    tags=""
    count=0
    tag = random.choice(hashtags)
    while count < 15:
        tag = random.choice(hashtags)
        if (tag not in tags):
            tags+="#"+tag+" "
            count=count+1
    return tags


# In[7]:


def formatPost(username, shortcode, text):
    output= ('------------------------- \n%s,  https://www.instagram.com/p/%s \n📸 and 📝 by @%s  \n%s' % 
             (username, 
              shortcode,
              username,
              text
             ))
    if (text.count('#') < 10):
        output+="\n"
        output+=randomHashtags()

    output+="\n\n\n\n"
    
    return output


# In[8]:


def getBest(username):
    print(username)

    file_string = ("ig_meta/"+username+".json")
    if os.path.isfile(file_string):
        with open(file_string) as json_data:
            d = json.load(json_data)

        most_likes = 0
        second_most_likes = 0

        # Search for best and second best posts
        for i in d['GraphImages']:
            if (i["edge_media_preview_like"]["count"] > most_likes):
                most_likes_meta = i
                most_likes = i["edge_media_preview_like"]["count"]
            elif (i["edge_media_preview_like"]["count"] > second_most_likes):
                second_most_likes_meta = i
                second_most_likes = i["edge_media_preview_like"]["count"]
        
        p = open("ig_meta/"+most_likes_meta["shortcode"]+".jpg",'wb')
        p.write(requests.get(most_likes_meta["display_url"]).content)
        p.close()

        p = open("ig_meta/"+second_most_likes_meta["shortcode"]+".jpg",'wb')
        p.write(requests.get(second_most_likes_meta["display_url"]).content)
        p.close()

        global output_log 
        output_log+= (username)
        output_log+= ('\nlike count %s, hashtag %s, https://www.instagram.com/p/%s' % 
                      (most_likes_meta["edge_media_preview_like"]["count"], 
                       most_likes_meta["edge_media_to_caption"]["edges"][0]['node']['text'].count('#'),
                       most_likes_meta["shortcode"]
                      ))
            
        output_log+= ('\nlike count %s, hashtag %s, https://www.instagram.com/p/%s' % 
                      (second_most_likes_meta["edge_media_preview_like"]["count"], 
                       second_most_likes_meta["edge_media_to_caption"]["edges"][0]['node']['text'].count('#'),
                       second_most_likes_meta["shortcode"]
                      ))
        output_log+= ('\n\n')
        
        global output_post_text 
        output_post_text+= formatPost(
            username,
            most_likes_meta["shortcode"],
            most_likes_meta["edge_media_to_caption"]["edges"][0]['node']['text'])
        
        output_post_text+= formatPost(
            username,
            second_most_likes_meta["shortcode"],
            second_most_likes_meta["edge_media_to_caption"]["edges"][0]['node']['text'])
                


# In[9]:


# print(formatPost(
#             "username",
#             "shortcode",
#             "test"))


# In[10]:


# Get list of users
with open('influencers.txt') as f:
    content = f.readlines()
content = [x.strip() for x in content] #remove whitespace characters


# In[11]:


for c in content:
    getBest(c)

f = open("downloads/bestPostsList.txt", "w")
f.write(output_log)
f.close()

f = open("downloads/bestPostsText.txt", "w")
f.write(output_post_text)
f.close()


# In[12]:


print(output_log)


# In[13]:


get_ipython().system('jupyter nbconvert --to script bestPostFinder.ipynb')


# In[ ]:




