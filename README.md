Instagram Scraper
=================
instagram-scraper is a command-line application written in Python that scrapes and downloads an instagram user's photos and videos.

Beginner Steps
--------------
Open up a command line on your computer. That is the ugly scary black window you see "hackers" use in the movies.

Mac: To do this on a Mac, search for the "terminal" application (should be in your utilities). Open that up and you should see a blank white screen with a curser.

Windows: On Windows, go to your Start menu. In the search bar, type "cmd" and hit enter. that will open the command window. It is likely to be a black screen. This is your terminal. Video walkthrough of this: https://www.youtube.com/watch?v=MBBWVgE0ewk

Now type the following to download the code (install git if there is a prompt for it)
```
git clone https://boulderbuff64@bitbucket.org/boulderbuff64/titan_ig_influencer_scraper.git
```

Download Python3
----------------
You need to download Python if you don't have it. Make sure to download Python3 and not Python2. It should be called Python 3.8.x or Python 3.9.x
Site to download Python: https://www.python.org/downloads/
Video walkthrough for Windows: https://www.youtube.com/watch?v=gFNApsyhpKk
Video walkthrough for Mac: https://youtu.be/0hGzGdRQeak?t=135



Install Scraper
-------
To install instagram-scraper:
```bash
$ pip install instagram-scraper
```

To update instagram-scraper:
```bash
$ pip install instagram-scraper --upgrade
```


Usage
-----

1. Open influencers.txt and add all your influencers

2. Open hashtags.txt and add all your hashtags

3. Scrape a data from Instagram influencers:
```bash
bash run-me.sh
```
*NOTE: To scrape a private user's media you must be an approved follower.*

4. Download the best photos and descriptions
```bash
python3 bestPostFinder.py
```

5. Use this info to post to creates posts on Later.com or Hootsuite.com
